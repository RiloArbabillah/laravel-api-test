## How to install

- composer install
- cp .env.example .env
- create your database and change the .env file to match your database name
- php artisan key:generate
- php artisan migrate:fresh --seed
    migrate the table and seed 10 data into struks table
- done


## Api Link for testing
- GET : api/struk
    get all data from strucks table

- POST : api/struk/create (key = name, value = any string)
    create a struck data
