<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/alat', 'StrukController@data')->name('Alat.index');
Route::get('/alat/create', 'StrukController@create')->name('Alat.create');
Route::post('/alat/store-manual', 'StrukController@store_manual')->name('Alat.store_manual');

Route::get('/alat/edit/{alat}', 'StrukController@edit')->name('Alat.edit');
Route::put('/alat/update/{alat}', 'StrukController@update')->name('Alat.update');
Route::delete('/alat/destroy/{alat}', 'StrukController@destroy')->name('Alat.destroy');
