@extends('layouts.home')
@section('content')
<div class="bg-gray-400 p-10 rounded-lg">
    <form action="{{ route('Alat.update', $stru->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="flex justify-left bg-blue-700 px-3 py-2 text-white mb-5 ">
        <a class="">
            Silahkan edit data
        </a>
    </div>
    <div class="flex flex-col text-blue-900">
        <table class="mt-2 bg-blue-300">ID Alat :</label>
        <input class="px-2 rounded-lg" type="text" name="id_alat"
        value="{{ $stru->id_alat}}">

        <table class="mt-2">Nama :</label>
        <input class="px-2 rounded-lg" type="text" name="nama"
        value="{{ $stru->nama}}">

        <table class="mt-2">Suhu :</label>
        <input class="px-2 rounded-lg" type="text" name="suhu"
        value="{{ $stru->suhu}}">

        <table class="mt-2">Keterangan :</label>
        <select name="keterangan">
            <option value="1">Sudah</option>
            <option value="2">Belum</option>
        </select>

    </div>

    <div>
        <button type="submit" class=" mt-4 bg-green-700 px-4 py-2 text-white rounded-lg hover:bg-green-900">
            Simpan
        </button>
    </div>
    </form>
</div>
@endsection
