@extends('layouts.home')
@section('content')
<div>
    <div>
        <div >
            <a href="{{route('Alat.create')}}" >
                Test Tambah Data
            </a>
        </div>
    </div>

    <table>
        <thead>
            <tr>
                <th>No</th>
                <th>ID Alat</th>
                <th>Nama</th>
                <th>Suhu</th>
                <th>Keterangan</th>
                <th>Created At</th>
                <th>Aksi</th>

            </tr>
        </thead>

     <tbody class="bg-blue-100 text-blue-800">
        @foreach ($struk as $index => $item)
        <tr class="text-center hover:bg-gray-400">
            <td class="border py-1">
                {{$index + 1}}
            </td>
            <td class="border py-1 px-3 text-left">
                {{$item->id_alat}}
            </td>
            <td class="border py-1 px-3">
                {{$item->nama}}
            </td>
            <td class="border py-1 px-3">
                {{$item->suhu}}
            </td>
            <td class="border py-1 px-3 capitalize">
                @if ($item->keterangan == 1)
                    sudah
                @else
                    belum
                @endif
            </td>
            <td class="border py-1 px-3">
                {{$item->created_at}}
            </td>

            <td class="border text-center flex space-x-4 justify-center text-white">
                <form action="{{route('Alat.destroy', $item->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="{{ route('Alat.edit', $item->id) }}"
                        class=" bg-blue-500 hover:bg-blue-800 rounded-lg px-4 py-1"
                        >
                        Edit
                    </a>
                    <button type="submit"
                        class="bg-red-500 hover:bg-red-700 rounded-lg px-2 py-1"
                        >
                        Hapus
                    </button>
               </form>
            </td>
        </tr>
        @endforeach
        @if ($struk->count() == 0)
        <tr>
            <td colspan="10" class="text-center p-4 text-red"> Tidak Ada Data </td>
        </tr>
        @endif

        </tbody>
    </table>
</div>
@endsection
