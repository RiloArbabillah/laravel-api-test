@extends('layouts.home')
@section('content')
<div class="bg-gray-400 p-10 rounded-lg ">
    <form action="{{ route('Alat.store_manual') }}" method="post">
    @csrf
    <div class="flex justify-left bg-blue-700 px-3 py-2 text-white mb-5">
        <a class="">
            Silahkan Input Data Manual
        </a>
    </div>
    <div class="flex flex-col text-blue-900">
        <label class="mt-2" >ID Alat :</label>
        <input id="id_alat" class="py-1 px-2 rounded-lg" type="text" name="id_alat">

        <label class="mt-2">Nama :</label>
        <input id="nama" class="py-1 px-2 rounded-lg" type="text" name="nama">

        <label class="mt-2">Suhu :</label>
        <input id="suhu" class="py-1 px-2 rounded-lg" type="text" name="suhu">

        <label class="mt-2">Keterangan :</label>
        <select name="keterangan" id="keterangan">
            <option value="1">Sudah</option>
            <option value="2">Belum</option>
        </select>


    </div>


    <div>
        <button type="submit" class=" mt-4 bg-green-700 px-4 py-2 text-white rounded-lg hover:bg-green-900">
            Simpan
        </button>


        {{-- @error('judul') <h1 class="text-red-500">{{$message}}</h1>@enderror       --}}
    </div>
    </form>
</div>
@endsection
