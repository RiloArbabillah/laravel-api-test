<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Project DEKA</title>

      {{-- untuk menggunkan tailwind --}}
      <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

      {{-- test --}}
      <link rel="dns-prefetch" href="//fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    </head>

    <body class="bg-gray-300 text-gray-700">
      <div class="flex flex-col h-screen">
        {{-- navbar --}}
        <div class="py-4 bg-blue-900 justify-center items-center h-32 text-white text-center">
          <a>Data Alat</a>
        </div>

        @if($errors->any())
            <div class="rounded-lg bg-red-500">
                @foreach ($errors->all(':message') as $item)
                    <div class="text-white text-center">
                        {{ $item }}
                    </div>
                @endforeach
            </div>
        @endif

        <div class="flex flex-grow  overflow-hidden">
            <div class="bg-gray-800 flex flex-col items-center h-screen space-y-7">
                <a class=" w-full py-2 px-20 rounded-lx bg-blue-700 text-white text-center border" >BUKU INDONESIA</a>
                <a href="{{ route('Alat.index') }}"
                  class=" w-full py-2 rounded-lx bg-gray-700 hover:bg-gray-900 text-white text-center" >Data Buku</a>

            </div>
             {{-- tempat untuk lokasi child/anak --}}
                <div class="overflow-y-auto p-10 w-full h-full">
                    @yield('content')
                </div>

        </div>
      </div>
    </body>
</html>
