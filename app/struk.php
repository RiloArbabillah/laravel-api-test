<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class struk extends Model
{
    //
    protected $table = 'struks';

    protected $fillable = [
        'nama', 'suhu', 'keterangan', 'id_alat', 'id_transaksi'
    ];
}
