<?php

namespace App\Http\Controllers;

use App\Struk;
use Illuminate\Support\Facades\Request;

class ApiStrukController extends Controller
{
    public function index()
    {
        $struk = Struk::all();
        return response()->json($struk, 200);
    }

    public function store()
    {
        $id_transaksi = request()->get('id_transaksi');
        $id_alat = request()->get('id_alat');
        $nama = request()->get('nama');
        $suhu = request()->get('suhu');
        $keterangan = request()->get('keterangan');

        $struk = Struk::create([
            'id_transaksi' => $id_transaksi,
            'nama' => $nama,
            'id_alat' => $id_alat,
            'suhu' => $suhu,
            'keterangan' => $keterangan,
        ]);

        return response()->json($struk);
    }
}
