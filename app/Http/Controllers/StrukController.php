<?php

namespace App\Http\Controllers;

use App\Struk;
use Illuminate\Support\Facades\Request;

class StrukController extends Controller
{
    public function data()
    {
        $struk = Struk::all();
        return view('Alat.index', compact('struk'));
    }

    public function index()
    {
        $struk = Struk::all();
        return view('Alat.index', compact('struk'));
    }

    public function create()
    {
        return view('Alat.form');
    }

    public function store_manual()
    {
        Request::validate([
            'nama' => 'required',
            'id_alat' => 'required',
            'suhu' => 'required',
            'keterangan' => 'required',
        ]);

        $nama = request()->get('nama');
        $id_alat = request()->get('id_alat');
        $suhu = request()->get('suhu');
        $keterangan = request()->get('keterangan');

        Struk::create([
            'nama' => $nama,
            'id_alat' => $id_alat,
            'suhu' => $suhu,
            'keterangan' => $keterangan,
        ]);

        return redirect()->route('Alat.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stru = Struk::find($id);
        return view('Alat.update', compact('stru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $nama = Request::get('nama');
        $id_alat = Request::get('id_alat');
        $suhu = Request::get('suhu');
        $keterangan = Request::get('keterangan');

        $stru = Struk::find($id);
        $stru->update([
            'nama' => $nama,
            'id_alat' => $id_alat,
            'suhu' => $suhu,
            'keterangan' => $keterangan,
        ]);
        return redirect()->route('Alat.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stru = Struk::find($id);

        if ($stru){
            $stru->delete();
            return redirect()->route('Alat.index')
            ->withSuccess('Data berhasil dihapus');
        }else{
            return redirect()->back()
            ->withErrors('Data tidak ditemukan');
        }
    }
}
