<?php

use Illuminate\Database\Seeder;
use App\Struk;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->struk();
    }

    public function struk()
    {
        for ($i=0; $i < 10; $i++) {
            Struk::create([
                'nama' => Str::random(10),
            ]);
        }
    }
}
