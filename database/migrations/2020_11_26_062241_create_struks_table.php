<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStruksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // alur
        // alat scan
        // selesai scan print struk dengan qr code dan upload data ke laravel
        // upload data ke laravel harus ada id mesin, suhu, status
        // qr harus memiliki id yg sama dengan yg ada di server
        // user login/register->login di hp lalu scan qr
        // update data server yg memiliki id sama dengan id di qr code
        // done

        Schema::create('struks', function (Blueprint $table) {
            $table->id('id');
            $table->string('id_alat');
            $table->string('id_transaksi');
            $table->string('nama')->nullable();
            $table->double('suhu')->nullable();
            $table->tinyInteger('keterangan')->nullable(); // 1 = sudah, 0 = belum
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('struks');
    }
}
