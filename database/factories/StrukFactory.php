<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\struk;
use Faker\Generator as Faker;

$factory->define(struk::class, function (Faker $faker) {
    return [
        'id' => $faker->id,
        'nama' => Str::random(10),
    ];
});
